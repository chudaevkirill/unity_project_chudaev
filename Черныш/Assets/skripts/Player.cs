﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Rigidbody rb;
    public bool jump;
    Animator animator;
    public int strips_nomber;
    public Game game;
    GameObject[] doors;
    GameObject[] stripss;

    public AudioClip Audio_fail;
    public AudioClip Audio_check;
    public AudioClip Audio_steps;
    public AudioClip Audio_jump;
    public AudioClip Audio_victory;
    public AudioClip Audio_lose;

    void Start()
    {
        stripss = GameObject.FindGameObjectsWithTag("strips");

        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        animator.SetBool("Jump", false);
        jump = false;

        if (doors == null)
            doors = GameObject.FindGameObjectsWithTag("door");
    }

   
   
    public void OnCollisionEnter(Collision collision)
    {
        animator.SetBool("Jump", false);
        jump = false;
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "strips")//получение номера текущей полосы
        {
            strips strips = other.GetComponent<strips>();
            strips_nomber = strips.nomber;
            for (int i = 0; i < stripss.Length; i++)//перебор линий чтобы восстановить выключеные
            {
                if (stripss[i] != null && strips_nomber > stripss[i].GetComponent<strips>().nomber && strips_nomber-5 < stripss[i].GetComponent<strips>().nomber)
                {
                    stripss[i].SetActive(true);
                }
            }


        }
        if (other.tag == "Finish")
        {
            GetComponent<AudioSource>().PlayOneShot(Audio_check, 0.3f);
            game.state = Game.GameState.VICTORY;
        }
        if (other.tag == "Check")
        {
            GetComponent<AudioSource>().PlayOneShot(Audio_check, 0.3f);
            game.Check = other.gameObject;
            game.destoyStrips();//уничтожаем преведушие полоски 
        }
        if (other.tag == "Fish")
        {
            Destroy(other.gameObject);
            game.fish_on_lvl+=1;
            GetComponent<AudioSource>().PlayOneShot(Audio_check, 0.3f);
            

        }
    }

    public bool Door_on_way()//есть ли дверь впереди
    {
        
        RaycastHit hit;
        Ray ray = new Ray(transform.position, Vector3.forward);
        Physics.Raycast(ray,out hit,3f);
        if (hit.collider != null)
        {
            if (hit.collider.gameObject.tag == "door")
            {
                return true;
            }
        }
        return false;

    }

}