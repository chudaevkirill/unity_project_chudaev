﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fonMusic : MonoBehaviour
{
    Game game;

    public void Start()
    {
        game = FindObjectOfType<Game>();
    }
    public void Update()
    {
        if (game.state == Game.GameState.VICTORY || game.state == Game.GameState.LOSE|| game.state == Game.GameState.PAUSE)
        {
            GetComponent<AudioSource>().Pause();
        }
        else if (!GetComponent<AudioSource>().isPlaying)
        { GetComponent<AudioSource>().Play(); }
    }
}
