﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Begin : MonoBehaviour
{
    GameObject camer;

    public Button startB;
    public Button ContinueB;
    public Button NewGameB;
    public void Start()
    {
        camer = this.gameObject;
        startB.gameObject.SetActive(true);
        ContinueB.gameObject.SetActive(false);
        NewGameB.gameObject.SetActive(false);
    }

    public void begin()
    {
        camer.GetComponent<Animator>().SetBool("start", true);
        startB.gameObject.SetActive(false);
    }
    public void NewGame()
    {
        PlayerPrefs.SetInt("LevelUnlock", 1);
        PlayerPrefs.SetInt("Fish", 0);
        PlayerPrefs.SetFloat("Speed", 4f);

        SceneManager.LoadScene(1);
    }
    public void Continue()
    {
        SceneManager.LoadScene(1);
    }
    public void begin2()
    {
        if (PlayerPrefs.HasKey("LevelUnlock") == false)
        {
            ContinueB.interactable = false;
        }
        ContinueB.gameObject.SetActive(true);
        NewGameB.gameObject.SetActive(true);
        
            
    }
}
