﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    public GameObject Pause;
    public GameObject Lose;
    public GameObject Victory;

    Game game;
    public void Start()
    {
        Pause.SetActive(false);
        Lose.SetActive(false);
        Victory.SetActive(false);

        game = FindObjectOfType<Game>();
    }

    public void pause()
    {
        if (game.state != Game.GameState.PAUSE)
        {
            Pause.SetActive(true);
            game.state = Game.GameState.PAUSE; 
        }
    }

    public void Continue()
    {
        if (game.state != Game.GameState.GAME)
        {
            Pause.SetActive(false);
            Lose.SetActive(false);
            Victory.SetActive(false);

            game.state = Game.GameState.GAME; 
        }

    }
    public void Menu()
    {
        SceneManager.LoadScene(1);
    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void Update()
    {
        if (game.state == Game.GameState.VICTORY)
        {
            Victory.SetActive(true);
        }
        else { Victory.SetActive(false); }
        if (game.state == Game.GameState.LOSE)
        {
            Lose.SetActive(true);
        }
        else { Lose.SetActive(false); }
    }

}
