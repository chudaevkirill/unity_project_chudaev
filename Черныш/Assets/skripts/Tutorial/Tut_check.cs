﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tut_check : MonoBehaviour
{
    public GameObject tut_check;


    public void Start()
    {
        tut_check.SetActive(false);
    }
    public void OnTriggerEnter(Collider other)
    {
        tut_check.SetActive(true);
    }
    public void OnTriggerExit(Collider other)
    {
        tut_check.SetActive(false);
    }
}
