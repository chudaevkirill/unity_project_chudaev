﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tut_Jump : MonoBehaviour
{
    public GameObject Tut_jump;


    public void Start()
    {
        Tut_jump.SetActive(false);
    }
    public void OnTriggerEnter(Collider other)
    {
        Tut_jump.SetActive(true);
    }
    public void OnTriggerExit(Collider other)
    {
        Tut_jump.SetActive(false);
    }
}
