﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tut_time : MonoBehaviour
{
    public GameObject tut_time;


    public void Start()
    {
        tut_time.SetActive(false);
    }
    public void OnTriggerStay(Collider other)
    {
        tut_time.SetActive(true);
    }
    public void OnTriggerExit(Collider other)
    {
        tut_time.SetActive(false);
    }
}
