﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tut_move : MonoBehaviour
{
    public GameObject tut_move;


    public void Start()
    {
        tut_move.SetActive(false);
    }
    public void OnTriggerStay(Collider other)
    {
        tut_move.SetActive(true);
    }
    public void OnTriggerExit(Collider other)
    {
        tut_move.SetActive(false);
    }
}
