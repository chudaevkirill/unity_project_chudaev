﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tut_door : MonoBehaviour
{
    public GameObject tut_door;


    public void Start()
    {
        tut_door.SetActive(false);
    }
    public void OnTriggerEnter(Collider other)
    {
        tut_door.SetActive(true);
    }
    public void OnTriggerExit(Collider other)
    {
        tut_door.SetActive(false);
    }
}
