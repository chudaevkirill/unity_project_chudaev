﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Status : MonoBehaviour
{
    public Text levelUnlock;
    public Text fish_coll;
    public Text speed;
    public Button Upgrade;
    void Start()
    {
        if (PlayerPrefs.HasKey("LevelUnlock") == true)
        {
            levelUnlock.text = "Unlocked "+ PlayerPrefs.GetInt("LevelUnlock") +" level";
            fish_coll.text = "" + PlayerPrefs.GetInt("Fish") + " Fish collected";
            speed.text = "Cat speed " + PlayerPrefs.GetFloat("Speed");
        }
    }
    public void Update()
    {
        if (PlayerPrefs.GetInt("Fish") < 20)
        {
            Upgrade.interactable = false;
        }
        if (PlayerPrefs.GetFloat("Speed") >= 10f)
        {
            Upgrade.interactable = false;
            Upgrade.GetComponentInChildren<Text>().text = "Gotta go fast";
        }
    }

    public void Buy_speed()
    {
        PlayerPrefs.SetInt("Fish", PlayerPrefs.GetInt("Fish")-20);
        PlayerPrefs.SetFloat("Speed", PlayerPrefs.GetFloat("Speed") + 0.5f);

        fish_coll.text = "" + PlayerPrefs.GetInt("Fish") + " Fish collected";
        speed.text = "Cat speed " + PlayerPrefs.GetFloat("Speed");
    }
}
