﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Levels : MonoBehaviour
{
    public int level;//куда должна перевести кнопка
    void Start()
    {
        Button B = GetComponent<Button>();
        if (level > 1)
        {
            B.interactable = false;
        }
        if (PlayerPrefs.HasKey("LevelUnlock") == true)
        {
            if (PlayerPrefs.GetInt("LevelUnlock") >= level)
            {
                B.interactable = true;
            }
        }
    }
    public void LoadScene()
    {
        SceneManager.LoadScene(level+1);//у нас 2 начальные сцены
    }
}

