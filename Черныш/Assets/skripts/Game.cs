﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    Joystick joystick;
    JoyJump jumpB;
    JoyDoor doorB;

    public Player player;
    public GameObject Camera;
    public Animator chernish_Animator;
    public bool left;//переменная для поворота

    public bool victory=false;
    public bool lose=false;

    float Speed_Player;

    public Text time_Text;
    public Text Fish;
    public int fish_on_lvl;

    public float timer;
    GameObject[] strips;
    public GameObject Check;

    PlayerFollowing player_following;


    public enum GameState
    {
        GAME,
        PAUSE,
        DOOR,
        VICTORY,
        LOSE,
        RESTART
    
    }

    public GameState state = GameState.GAME;

    public void destoyStrips()
    {
        for (int i = 0; i < strips.Length; i++)//перебор линий чтобы уничтожить ненужные
        {
            if (strips[i] != null && player.strips_nomber < strips[i].GetComponent<strips>().nomber)
            {
                Destroy(strips[i].gameObject);
            }
        }
    }
    private void flip()//поворот игрока
    {
        left = !left;
        Vector3 chernScale = player.transform.localScale;
        chernScale.x *= -1;
        player.transform.localScale = chernScale;
    }
    IEnumerator door()//переход через дверь
    {
        yield return new WaitForSeconds(0.1f);
        player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z + 2f);
        state = Game.GameState.GAME;

    }

    void Start()
    {
        fish_on_lvl = PlayerPrefs.GetInt("Fish");
        Speed_Player = PlayerPrefs.GetFloat("Speed");

        if (SceneManager.GetActiveScene().name == "Tutorial")
        { Speed_Player = 8f; }

        strips = GameObject.FindGameObjectsWithTag("strips");
        joystick = FindObjectOfType<Joystick>();
        jumpB = FindObjectOfType<JoyJump>();
        doorB = FindObjectOfType<JoyDoor>();

        player.transform.position = Check.transform.position - new Vector3(0f, 0f, 1f);
        player.strips_nomber = Check.GetComponentInParent<strips>().nomber;

        state = GameState.GAME;
        left = false;
        player_following = Camera.AddComponent<PlayerFollowing>();
        player_following.Init(player.gameObject);

    }
    
    
    void FixedUpdate()
    {
        switch (state)
        {
            case GameState.GAME:
               
                player.transform.position += new Vector3((joystick.Horizontal + Input.GetAxis("Horizontal")) * Time.deltaTime * Speed_Player, 0.0f, 0.0f);

                if (joystick.Horizontal != 0)//анимация движения
                {
                    chernish_Animator.SetBool("Run", true);

                    if (!player.GetComponent<AudioSource>().isPlaying)
                    { player.GetComponent<AudioSource>().PlayOneShot(player.Audio_steps, 0.45f); }
                }
                else
                {
                    chernish_Animator.SetBool("Run", false);
                }
                //повороты
                if ((joystick.Horizontal + Input.GetAxis("Horizontal")) < 0  && !left)
                {
                    flip();
                }
                else if ((joystick.Horizontal + Input.GetAxis("Horizontal")) > 0  && left)
                {
                    flip();
                }
                //если упал за пределы
                if (player.transform.position.y <= -2)
                {
                    state = GameState.RESTART; 
                }
                if (jumpB.pressed && player.jump == false)//прыжок
                {
                    player.GetComponent<AudioSource>().PlayOneShot(player.Audio_jump, 0.3f);

                    chernish_Animator.SetBool("Jump", true);
                    player.GetComponent<Rigidbody>().AddForce(0, 100, 0, ForceMode.Impulse);
                    player.jump = true;

                }
                if (doorB.pressed && player.Door_on_way())//вход в дверь
                {
                    player.GetComponent<AudioSource>().PlayOneShot(player.Audio_jump, 0.3f);
                    state = Game.GameState.DOOR;
                    StartCoroutine(door());
                }

                Fish.text = "All fish " + fish_on_lvl;

                if (timer >= 0)//отсчёт времени
                {

                    timer -= 1 * Time.deltaTime;
                    time_Text.text= "Time left: " + string.Format("{0:0.#}", timer);
                }
                else
                { state = GameState.LOSE; }

                break;
            case GameState.PAUSE:
                break;
            case GameState.DOOR:
                break;

            case GameState.RESTART:
                player.GetComponent<AudioSource>().PlayOneShot(player.Audio_fail, 0.3f);
                Check.transform.parent.gameObject.SetActive(true);
                player.strips_nomber = Check.GetComponentInParent<strips>().nomber;
                player.transform.position = Check.transform.position - new Vector3(0f, 0f, 1f);

                for (int i = 0; i < strips.Length; i++)//перебор линий чтобы восстановить выключеные
                {
                    if (strips[i] != null && player.strips_nomber > strips[i].GetComponent<strips>().nomber)
                    { 
                        strips[i].SetActive(true); 
                    }
                }

                state = GameState.GAME;
                break;

            case GameState.VICTORY:
                if (!victory)
                {
                    player.GetComponent<AudioSource>().PlayOneShot(player.Audio_victory, 0.6f);
                    victory = true;
                }
                    
                if (PlayerPrefs.HasKey("LevelUnlock") == true)
                {
                    if (PlayerPrefs.GetInt("LevelUnlock") <= SceneManager.GetActiveScene().buildIndex)
                    { PlayerPrefs.SetInt("LevelUnlock", SceneManager.GetActiveScene().buildIndex); }
                    
                }
                PlayerPrefs.SetInt("Fish", fish_on_lvl+30);
                chernish_Animator.SetBool("Jump", false);
                chernish_Animator.SetBool("Run", false);
                break;

            case GameState.LOSE:
                if (!lose)
                {
                    player.GetComponent<AudioSource>().PlayOneShot(player.Audio_lose, 0.6f);
                    lose = true;
                }
                PlayerPrefs.SetInt("Fish", fish_on_lvl);
                chernish_Animator.SetBool("Jump", false);
                chernish_Animator.SetBool("Run", false);
                break;
            default:
                break;


        }
        

    }
   
}
