﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollowing : MonoBehaviour
{

    float speed = 0.07f;
    GameObject target;
    Vector3 offset;

    public void Init(GameObject target_)
    {
        target = target_;
        offset = transform.position - target.transform.position;
    }

    void FixedUpdate()
    {
        Vector3 target_pos = target.transform.position;

        Vector3 desired_pos = target_pos + offset;
        transform.position = Vector3.Lerp(transform.position, desired_pos, speed);
        transform.LookAt(target_pos);
    }
}
